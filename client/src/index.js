//@flow
//eslint eqeqeq: "off"

import * as React from 'react';
import {Component} from 'react-simplified';
import {HashRouter, Route, NavLink} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {Alert, Card, NavBar, Button, Row, Column} from './widgets';
import {Sak, sakService} from './services';

// Reload application when not in production environment
if (process.env.NODE_ENV != 'production') {
    let script = document.createElement('script');
    script.src = '/reload/reload.js';
    if (document.body) document.body.appendChild(script);
}


import { createHashHistory } from 'history';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

let ulStyle = {
    listStyle: "none"
};


class Menu extends Component {
    render() {
        return (
            <NavBar brand="Abben50">
                <NavBar.Link to='/sak/ny'>Legg til ny sak</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Sport'>Sport</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Teknologi'>Teknologi</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Musikk'>Musikk</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Bil'>Bil</NavBar.Link>
            </NavBar>
        );
    }
}


class LiveFeed extends Component {
    saker: Sak[] = [];

    render() {
        return (
            <div style={{
                height: '162px',
                width: '80%',
                border: '1px solid #ccc',
                overflowY: 'hidden',
                whiteSpace: 'nowrap',
                margin: 'auto',
                marginBottom: '12px'
            }}>
                {this.saker.map(sak => (
                    <div className={"card"} style={{height: '180px', width: '40rem', display: 'inline-block'}}>
                        <div className="card-body">
                            <h5 className="card-title">{sak.overskrift}</h5>
                            <p className="card-text">{sak.tidsstempel}</p>
                            <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Les mer</NavLink>
                        </div>
                    </div>
                ))}
            </div>
        );
    }

    componentDidMount() {
        sakService
            .getNewestSaker()
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
        this.interval = setInterval(() => {sakService
            .getNewestSaker()
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
        }, 10000)
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
}


class MainFeed extends Component {
    saker: Sak[] = [];

    render() {

        return (
            <div>

                <div className="container">
                    <div className="row">
                        <div className="col-">
                        </div>
                        <div className="col-lg">
                            <ul style={{listStyle: "none"}}>
                                {this.saker.map((sak) => (
                                    <li key={sak.sak_id}>
                                        <br></br>
                                        <div className={"card"} style={{width: '70rem'}}>
                                            <img src={sak.bildelink} className={"card-img-top"} alt={sak.alt_tekst}/>
                                            <div className="card-body">
                                                <h5 className="card-title">{sak.overskrift}</h5>
                                                <p className="card-text">{sak.tidsstempel}</p>
                                                <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Les mer</NavLink>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="col-">
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        sakService
            .getSaker()
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
    }
}


class SakDetails extends Component<{ match: { params: { id: number } } }> {

    saker: Sak[] = [];

    render() {


        let sak = this.saker.find(sak => sak.sak_id == this.props.match.params.id);
        if (!sak) {
            console.error('sak ikke funnet');
            return null;
        }
        return (

            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <div className="card">
                            <img src={sak.bildelink} className="card-img-top" alt={sak.alt_tekst}/>
                            <div className="card-body">
                                <h5 className="card-title">{sak.overskrift}</h5>
                                <p className="card-text">{sak.innhold}</p>
                                <p className="card-text"><small className="text-muted">{sak.tidsstempel}</small></p>
                                <p className="card-text"><small className="text-muted">Artikkelnr.: {sak.sak_id}</small>
                                </p>
                                <NavLink className="btn btn-secondary"
                                         to={'/sak/rediger/' + sak.sak_id}>Rediger</NavLink>
                                <NavLink className="btn btn-danger" to={'/sak/slett/' + sak.sak_id}>Slett</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-"></div>
            </div>
        );
    }

    mounted() {
        sakService
            .getSaker(this.props.match.params.id)
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
    }
}

class NySak extends Component {


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <form>
                            <h1>Legg til ny sak</h1>
                            <br></br>
                            <div className="form-group">
                                <label htmlFor="overskriftInput">Overskrift:</label>
                                <input type="text" className="form-control" id="overskriftInput"
                                       aria-describedby="overskriftHelp" placeholder="Skriv tittel"></input>
                                <small id="overskriftHelp" className="form-text text-muted">Tittelen på
                                    artikkelen.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="innholdInput">Innhold:</label>
                                <input type="text" className="form-control" id="innholdInput"
                                       aria-describedby="innholdHjelp" placeholder="Skriv innhold"></input>
                                <small id="innholdHjelp" className="form-text text-muted">Maks
                                    255 tegn.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bildelinkInput">Bildelink:</label>
                                <input type="text" className="form-control" id="bildelinkInput"
                                       aria-describedby="bildeLinkHjelp"
                                       placeholder="Lim inn url"></input>
                                <small id="bildeLinkHjelp" className="form-text text-muted">url til bilde.</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="kategoriInput">Kategori:</label>
                                <input type="text" className="form-control" id="kategoriInput"
                                       aria-describedby="kategoriHjelp" placeholder="Skriv inn kategori"></input>
                                <small id="kategoriHjelp" className="form-text text-muted"> Sport, Teknologi, Musikk eller Bil</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="alt_tekstInput">Alternativ tekst:</label>
                                <input type="text" className="form-control" id="alt_tekstInput"
                                       aria-describedby="alt_tekstHjelp"
                                       placeholder="Skriv alternativ tekst"></input>
                                <small id="alt_tekstHjelp" className="form-text text-muted">Alternativ tekst</small>
                            </div>


                            <div className="form-group">
                                <label htmlFor="viktighetInput">Viktighet:</label>
                                <input type="text" className="form-control" id="viktighetInput"
                                       aria-describedby="viktighetHjelp" placeholder="Skriv inn viktighet"></input>
                                <small id="viktighetHjelp" className="form-text text-muted">
                                    Artikler med viktighet 1 vises på hovedsiden.</small>
                            </div>

                            <Button.Danger onClick={this.save}>Send</Button.Danger>
                            <NavLink className="btn btn-secondary" to={'/'}>Avbryt</NavLink>


                        </form>
                    </div>
                </div>
                <div className="col-"></div>
            </div>
        );
    }



    save() {
        let sak = new Sak(document.getElementById("overskriftInput").value, document.getElementById("innholdInput").value, document.getElementById("bildelinkInput").value, document.getElementById("kategoriInput").value, document.getElementById("alt_tekstInput").value, document.getElementById("viktighetInput").value)
        sakService
            .uploadSak(sak)
            .then(() => history.push('/'))
            .catch((error: Error) => Alert.danger(error.message));
        {console.log("sak sendt")}
    }
}





class SlettSak extends Component<{ match: { params: { id: number } } }> {
    sak = null;

    render() {

        if (!this.sak) return null;
        return (
            <div>

            </div>

        );
    }
    mounted() {
        sakService
            .slettSak(this.props.match.params.id)
            .catch((error: Error) => Alert.danger(error.message));
    }
}

class RedigerSak extends Component<{ match: { params: { id: number} } }> {
    sak = null;

    render() {


        if (!this.sak) return null;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <form>
                            <h1>Rediger sak {this.sak.sak_id}</h1>
                            <br></br>
                            <div className="form-group">
                                <label htmlFor="overskriftInput">Overskrift:</label>
                                <input type="text" className="form-control" id="overskriftInput"
                                       aria-describedby="overskriftHelp" value={this.sak.overskrift} onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    if (this.sak) this.sak.overskrift = event.target.value;}}></input>
                                <small id="overskriftHelp" className="form-text text-muted">Ønsket tittel</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="innholdInput">Innhold:</label>
                                <input type="text" className="form-control" id="innholdInput"
                                       aria-describedby="innholdHjelp" value={this.sak.innhold} onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    if (this.sak) this.sak.innhold = event.target.value;}}></input>
                                <small id="innholdHjelp" className="form-text text-muted">Maks
                                    255 tegn.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bildelinkInput">Bildelink:</label>
                                <input type="text" className="form-control" id="bildelinkInput"
                                       aria-describedby="bildeLinkHjelp" value={this.sak.bildelink} onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    if (this.sak) this.sak.bildelink = event.target.value;}}></input>
                                <small id="bildeLinkHjelp" className="form-text text-muted">url til bilde</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="kategoriInput">Kategori:</label>
                                <input type="text" className="form-control" id="kategoriInput"
                                       aria-describedby="kategoriHjelp" value={this.sak.kategori} onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    if (this.sak) this.sak.kategori = event.target.value;}}></input>
                                <small id="kategoriHjelp" className="form-text text-muted">Sport, Teknologi, Musikk eller Bil</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="viktighetInput">Viktighet:</label>
                                <input type="text" className="form-control" id="viktighetInput"
                                       aria-describedby="viktighetHjelp" value={this.sak.viktighet} onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    if (this.sak) this.sak.viktighet = event.target.value;}}></input>
                                <small id="viktighetHjelp" className="form-text text-muted">
                                    Artikler med viktighet 1 vises på hovedsiden.</small>
                            </div>

                            <Button.Danger onClick = {this.save}>Oppdater</Button.Danger>
                            <NavLink className="btn btn-secondary" to={'/sak/' + this.sak.sak_id}>Avbryt</NavLink>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        // this.sak = null;
        sakService
            .getSak(this.props.match.params.id)
            // .then(sak=>console.log(sak))
            .then(sak => (this.sak = sak))
            .catch((error: Error) => Alert.danger(error.message));

    }

    save() {
        if (!this.sak) return null;

        sakService
            .updateSak(this.sak)
            .then(() => {
                let sakListe = MainFeed.instance();
                if (sakListe) sakListe.mounted(); //update sak list
                if (this.sak) history.push('/sak/' + this.sak.sak_id);
            })
            .catch((error: Error) => Alert.danger(error.message));
    }
}



class CategoryFeed extends Component<{ match: { params: { kategori: string } } }> {
    saker: Sak[] = [];

    render() {

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-">
                        </div>
                        <div className="col-lg">
                            <ul style={{listStyle: "none"}}>
                                {this.saker.map((sak) => (
                                    <li key={sak.sak_id}>
                                        <br></br>
                                        <div className={"card"} activeStyle={"width: 18rem;"}>
                                            <img src={sak.bildelink} className={"card-img-top"} alt={sak.alt_tekst}/>
                                            <div className="card-body">
                                                <h5 className="card-title">{sak.overskrift}</h5>
                                                <p className="card-text">{sak.tidsstempel}</p>
                                                <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Gå til
                                                    sak</NavLink>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="col-">
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        sakService
            .getCategory(this.props.match.params.kategori)
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
    }
}


const root = document.getElementById('root');
if (root)
    ReactDOM.render(
        <HashRouter>
            <div>
                <Route path={"/"} component={Menu}/>
                <Route exact path={'/'} component={LiveFeed}/>
                <Route exact path={"/"} component={MainFeed}/>
                <Route exact path={"/sak/:id"} component={SakDetails}/>
                <Route exact path={"/sak/ny"} component={NySak}/>
                <Route exact path={'/sak/slett/:id'} component={SlettSak}/>
                <Route exact path={'/sak/rediger/:id'} component={RedigerSak}/>
                <Route exact path={'/sak/kategori/:kategori'} component={CategoryFeed}/>
            </div>
        </HashRouter>,
        root
    );
