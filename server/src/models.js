// @flow

import Sequelize from 'sequelize';
import type { Model } from 'sequelize';

// Connect to mysql-database
let sequelize = new Sequelize('abilashy', 'abilashy', 'pJ5ppR5C', {
    host: process.env.CI ? 'mysql.stud.iie.ntnu.no' : 'mysql.stud.iie.ntnu.no', // The host is 'mysql' when running in gitlab CI
    dialect: 'mysql',
    define: {freezeTableName: true}
});


export type Sak = {
    sak_id?: number,
    overskrift: string,
    innhold: string,
    tidsstempel: string,
    bildelink: string,
    kategori: string,
    alt_tekst: string,
    viktighet: number
};

// Create database table sak
export let SakerModel: Class<Model<Sak>> = sequelize.define('Saker', {
    sak_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    overskrift: Sequelize.STRING,
    innhold: Sequelize.STRING,
    tidsstempel: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    bildelink: Sequelize.STRING,
    kategori: Sequelize.STRING,
    alt_tekst: Sequelize.STRING,
    viktighet: Sequelize.INTEGER
});

// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';
// The sync promise can be used to wait for the database to be ready (for instance in your tests)
export let syncModels = sequelize.sync({ force: production ? false : true }).then(() => {
    // Create test data when not in production environment
    if (!production)
        return SakerModel.create({
            overskrift: 'BMW 850I',
            innhold: 'BMW 850I',
            bildelink: 'https://motor.elpais.com/wp-content/uploads/2019/04/BMW-Serie-8-2.jpg',
            kategori: 'Bil',
            alt_tekst: 'BMW 850I',
            viktighet: 1
        })
        .then(() =>
          SakerModel.create({
            overskrift: 'Liverpool',
            innhold: 'Liverpool',
            bildelink: 'https://upload.wikimedia.org/wikipedia/commons/0/00/Liverpool_vs._Chelsea%2C_UEFA_Super_Cup_2019-08-14_53.jpg',
            kategori: 'Sport',
            alt_tekst: 'Liverpool',
            viktighet: 1
        })
    );
});
